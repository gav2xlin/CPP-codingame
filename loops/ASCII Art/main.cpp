#include <cctype>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
    int l;
    cin >> l; cin.ignore();
    int h;
    cin >> h; cin.ignore();
    string t;
    getline(cin, t);

    vector<int> pos(t.length());
    for (int i = 0; i < t.length(); ++i) {
        char ch = ::toupper(t[i]);
        pos[i] = (('A' <= ch && ch <= 'Z') ? ch - 'A' : 26) * l;
    }

    for (int i = 0; i < h; i++) {
        string row;
        getline(cin, row);

        for (int j = 0; j < t.length(); ++j) {
            for (int k = 0; k < l; ++k) {
                cout << row[pos[j] + k];
            }
        }
        cout << endl;
    }
}
